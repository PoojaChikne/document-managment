$( document ).on( {
	ajaxStart: function() {
		$( "body" ).addClass( "loading" );
	},
	ajaxStop: function() {
		$( "body" ).removeClass( "loading" );
	}
} );

function addError( errorMesssage ) {
	var node = document.createElement( "LI" );
	var textnode = document.createTextNode( errorMesssage );
	node.appendChild( textnode );
	document.getElementById( "error-message" ).appendChild( node );
}

function hideError( element ) {
	element.classList.add( "invisible" );
	element.classList.remove( "visible" );
}

function showError( element ) {
	element.classList.add( "visible" );
	element.classList.remove( "invisible" );
	$( '#error-message' ).css( 'display', 'block' );
	$( '#error-message' ).delay( 5000 ).fadeOut();
}

function submitForm() {
	var uplodedFile = document.getElementById( "upload_file" );
	var fileSize = 0;
	var element = document.getElementById( "error-message" );
	var files = $( '#upload_file' )[0].files;
	$( "#error-message" ).html( "" );
	hideError( element );
	if( typeof (uplodedFile.files) != "undefined" ) {
		var fileSize = parseFloat( uplodedFile.files[0].size / (1024 * 1024) ).toFixed( 2 );
	}
	if( fileSize <= 0 || fileSize > 8 ) {
		addError( "Please select file size less than 8MB" );
		showError( element );
		// document.getElementById("error-message").display('block');
	} else {
		var formData = new FormData();
		formData.append( 'file', files[0] );
		$.ajax( {
			type: "POST",
			dataType: "json",
			url: "dashboard/uploadDocument",
			enctype: "multipart/form-data",
			data: formData,
			contentType: false,
			processData: false,
			success: function( response ) {
				response = JSON.parse( JSON.stringify( response ) );
				if( typeof response.success !== 'undefined' ) {
					addError( "File uploded sucessfully!" );
					showError( element );
					$( "#list_files" ).html( response.success );
				} else {
					$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
					addError( response.error );
					showError( element );
				}
			},
		} );
	}
}

function deleteFile( fileId ) {
	if( confirm( 'Are you sure you want to delete the file?' ) ) {
		var element = document.getElementById( "error-message" );
		$( "#error-message" ).html( "" );
		hideError( element );
		$.ajax( {
			type: "POST",
			dataType: "json",
			url: "dashboard/deleteDocument",
			data: { "id": fileId },
			success: function( response ) {
				response = JSON.parse( JSON.stringify( response ) );
				if( typeof response.success !== 'undefined' ) {
					addError( "File deleted sucessfully!" );
					showError( element );
					$( "#list_files" ).html( response.success );
				} else {
					$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
					addError( response.error );
					showError( element );
				}
			},
			error: function( response ) {
				$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
				addError( JSON.parse( JSON.stringify( response ) ).error );
				showError( element );
			}
		} );
	}
}

function handle( e ) {
	if( e.keyCode === 13 ) {
		e.preventDefault(); // Ensure it is only this code that runs
		var search = document.getElementById( 'keywords' ).value;
		var element = document.getElementById( "error-message" );
		$( "#error-message" ).html( "" );
		$.ajax( {
			type: "POST",
			dataType: "json",
			url: "dashboard/searchDocument",
			data: { "search": document.getElementById( 'keywords' ).value },
			success: function( response ) {
				response = JSON.parse( JSON.stringify( response ) );
				if( typeof response.success !== 'undefined' ) {
					$( "#list_files" ).html( response.success );
				} else {
					$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
					addError( response.error );
					showError( element );
				}
			},
			error: function( response ) {
				$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
				addError( JSON.parse( JSON.stringify( response ) ).error );
				showError( element );
			}
		} );
	}
}

function editFilename( fileId ) {
	Array.from( document.getElementsByClassName( "showelement" ) ).forEach(
		function( element, index, array ) {
			$( '#' + element.id ).show();
			if( element.type == "input" || element.type == 'text' ) {
				element.setAttribute( "type", "hidden" );
			}
		}
	);
	$( '#filename_' + fileId ).hide();
	$( '#edit_' + fileId ).hide();
	$( '#filename_' + fileId ).addClass( 'showelement' );
	$( '#edit_' + fileId ).addClass( 'showelement' );
	$( '#' + fileId ).addClass( 'showelement' );
	document.getElementById( fileId ).setAttribute( 'type', 'text' );
}

function updateFileName( e, id ) {
	if( e.keyCode === 13 ) {
		e.preventDefault();
		var element = document.getElementById( "error-message" );
		$( "#error-message" ).html( "" );
		$.ajax( {
			type: "POST",
			dataType: "json",
			url: "dashboard/updateFilename",
			data: {
				"id": id,
				"name": document.getElementById( id ).value
			},
			success: function( response ) {
				response = JSON.parse( JSON.stringify( response ) );
				if( typeof response.success !== 'undefined' ) {
					$( "#list_files" ).html( response.success );
				} else {
					$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
					addError( response.error );
					showError( element );
				}
			},
			error: function( response ) {
				$( "html, body" ).animate( { scrollTop: 0 }, "slow" );
				addError( JSON.parse( JSON.stringify( response ) ).error );
				showError( element );
			}
		} )
	}
}

