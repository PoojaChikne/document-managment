<div class="container">
	<div class="row">
		<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper text-left">
			<div class="container">
				<h3>Register</h3>
				<hr>
				<form class="" action="/register" method="post">
					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label for="firstname">First Name</label>
								<input type="text" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'firstname' ) ) ? 'is-invalid' : ''; ?>" name="firstname" id="firstname" value="<?= set_value( 'firstname' ) ?>">
								<?php
								if( isset( $validation ) && $validation->hasError( 'firstname' ) ) {
									echo '<p class="invalid-feedback">' . $validation->getError( 'firstname' ) . '</p>';
								}
								?>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label for="lastname">Last Name</label>
								<input type="text" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'lastname' ) ) ? 'is-invalid' : ''; ?>" name="lastname" id="lastname" value="<?= set_value( 'lastname' ) ?>">
								<?php
								if( isset( $validation ) && $validation->hasError( 'lastname' ) ) {
									echo '<p class="invalid-feedback">' . $validation->getError( 'lastname' ) . '</p>';
								}
								?>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="email">Email address</label>
								<input type="text" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'email' ) ) ? 'is-invalid' : ''; ?>" name="email" id="email" value="<?= set_value( 'email' ) ?>">
								<?php
								if( isset( $validation ) && $validation->hasError( 'email' ) ) {
									echo '<p class="invalid-feedback">' . $validation->getError( 'email' ) . '</p>';
								}
								?>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'password' ) ) ? 'is-invalid' : ''; ?>" name="password" id="password" value="">
								<?php
								if( isset( $validation ) && $validation->hasError( 'password' ) ) {
									echo '<p class="invalid-feedback">' . $validation->getError( 'password' ) . '</p>';
								}
								?>
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label for="password_confirm">Confirm Password</label>
								<input type="password" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'password_confirm' ) ) ? 'is-invalid' : ''; ?>" name="password_confirm" id="password_confirm" value="">
								<?php
								if( isset( $validation ) && $validation->hasError( 'password_confirm' ) ) {
									echo '<p class="invalid-feedback">' . $validation->getError( 'password_confirm' ) . '</p>';
								}
								?>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-12 col-sm-4">
							<div class="button_outer">
								<div class="btn_upload">
									<input type="submit" />Register
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-8 text-right">
							<a href="/">Already have an account</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>