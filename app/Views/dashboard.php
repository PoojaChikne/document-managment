<div class="bg-white shadow-sm">
	<div class="container pl">
		<div class="row">
			<nav class="nav nav-underline">
				<div class="nav-link">Hello, <?= session()->get( 'firstname' ); ?></div>
			</nav>
		</div>
	</div>
</div>
<div class="container mt-4">
	<div class="row">
		<div class="col-md-12 text-right pr">
			<form class="uploadform" method="post" enctype="multipart/form-data" action='dashboard/uploadDocument'>
				<div class="button_outer">
					<div class="btn_upload">
						<input type="file" id="upload_file" name="upload_file" onClick="this.form.reset()" onchange="submitForm()" />Upload
						File
					</div>
				</div>
			</form>
			<?php if( session()->get( 'error' ) ) { ?>
				<div class="alert alert-error" role="alert">
					<?php echo session()->get( 'error' ) ?>
				</div>
			<?php } ?>
			<div class="errors error_msg invisible" role="alert" id="error-message"></div>
			<div class="overlay"></div>
		</div>
	</div>
	<div>
		<div class="container mt-4">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-dark shadow-sm">
							<div class="row">
								<div class="card-header-text text-white col-md-6 text-left">Documents</div>
								<form action="#" class="text-right col-md-6">
									<input type="text" class="shadow-sm" name="keywords" id="keywords" placeholder="search and enter" onkeypress="handle(event)" />
								</form>
							</div>
						</div>
						<div class="class-body">
							<div id="main-content" class="file_manager mt-4">
								<div class="container">
									<div class="row clearfix" id="list_files">
										<?php if( isset( $files ) && !empty( $files ) ) {
											echo view( 'list', $files );
										} else { ?>
											<div class="col-lg-3 col-md-4 col-sm-12"><span>No Files Available</span>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
