<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<?php if( isset( $files ) && !empty( $files ) ) {
	foreach( $files as $file ) { ?>
		<div class="col-lg-3 col-md-4 col-sm-12">
			<div class="card shadow-sm">
				<div class="file">
					<a href="javascript:void(0);">
						<div class="hover">
							<button type="button" class="btn btn-icon btn-success" id="edit_<?php echo $file['id']; ?>" onclick="editFilename('<?php echo $file['id']; ?>')">
								<i class="fa fa-pencil"></i>
							</button>
							<button type="button" class="btn btn-icon btn-warning" onclick="document.getElementById('form'+'<?php echo $file['id']; ?>').submit();">
								<i class="fa fa-arrow-down"></i>
							</button>
							<button type="button" class="btn btn-icon btn-danger" id="delete_<?php echo $file['id']; ?>" onclick="deleteFile(<?php echo $file['id']; ?>)">
								<i class="fa fa-trash"></i>
							</button>
						</div>
						<div class="icon">
							<i class="fa fa-file text-info"></i>
						</div>
						<div class="file-name">
							<p class="m-b-5 text-muted">
								<span id="filename_<?php echo $file['id']; ?>"><?php echo $file['name']; ?></span>
								<input type="hidden" name="filepath" value="<?php echo pathinfo( $file['name'], PATHINFO_FILENAME ); ?>" id="<?php echo $file['id']; ?>" onkeypress="updateFileName(event,'<?php echo $file['id']; ?>')">
							</p>
							<small>Size: <?php echo $file['file_size']; ?> MB
								<span class="date text-muted"> <?php echo $file['ext']; ?></span></small>
						</div>
						<form method="POST" action='dashboard/downloadFile' id="form<?php echo $file['id']; ?>">
							<input type="hidden" name="id" value="<?php echo $file['id']; ?>">
						</form>
					</a>
				</div>
			</div>
		</div>
	<?php }
} else { ?>
	<div class="col-lg-3 col-md-4 col-sm-12"><span>No Files Available</span></div>
<?php } ?>