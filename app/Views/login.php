<div class="container">
	<div class="row">
		<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper text-left">
			<div class="container">
				<h3>Login</h3>
				<hr>
				<?php if( session()->get( 'success' ) ): ?>
					<div class="alert alert-success" role="alert">
						<?= session()->get( 'success' ) ?>
					</div>
				<?php endif; ?>
				<form class="" action="/" method="post">
					<div class="form-group">
						<label for="email">Email address</label>
						<input type="text" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'email' ) ) ? 'is-invalid' : ''; ?>" name="email" id="email" value="<?= set_value( 'email' ) ?>">
						<?php
						if( isset( $validation ) && $validation->hasError( 'email' ) ) {
							echo '<p class="invalid-feedback">' . $validation->getError( 'email' ) . '</p>';
						}
						?>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control <?php echo ( isset( $validation ) && $validation->hasError( 'password' ) ) ? 'is-invalid' : ''; ?>" name="password" id="password" value="">
						<?php
						if( isset( $validation ) && $validation->hasError( 'password' ) ) {
							echo '<p class="invalid-feedback">' . $validation->getError( 'password' ) . '</p>';
						}
						?>
					</div>
					<div class="row">
						<div class="col-12 col-sm-4">
							<div class="button_outer">
								<div class="btn_upload">
									<input type="submit" />Login
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-8 text-right">
							<a href="/register">Don't have an account yet?</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>