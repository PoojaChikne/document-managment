<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="/assets/css/custom.css">
        <title>Document Managment</title>
    </head>
    <body>
    <?php
      $uri = service('uri');
     ?>
  <div class="container-fluid bg-dark shadow-sm">
    <div class="container pb-2 pt-2">
      <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <i class="bi bi-file-text"></i>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarContent">
            <?php if (session()->get('isLoggedIn')): ?>
              <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= ($uri->getSegment(1) == 'dashboard' ? 'active' : null) ?>">
                  <a class="nav-link"  href="/dashboard">Dashboard</a>
                </li>
              </ul>
              <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item">
                  <a class="nav-link" href="/logout">Logout</a>
                </li>
              </ul>
            <?php else: ?>
              <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= ($uri->getSegment(1) == '' ? 'active' : null) ?>">
                  <a class="nav-link" href="/">Login</a>
                </li>
                <li class="nav-item <?= ($uri->getSegment(1) == 'register' ? 'active' : null) ?>">
                  <a class="nav-link" href="/register">Register</a>
                </li>
              </ul>
              <?php endif; ?>
          </div>
        </div>
      </nav>
    </div>
  </div>
     