<?php namespace App\Models;

use CodeIgniter\Model;

class Files extends Model {

	protected $table = 'files';
	protected $primaryKey = 'id';
	protected $allowedFields = [ 'user_id', 'name', 'file_size', 'ext', 'file_path' ];

}