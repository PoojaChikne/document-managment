<?php namespace App\Models;

use CodeIgniter\Model;

class User extends Model {

	protected $table = 'user';
	protected $allowedFields = [ 'firstname', 'lastname', 'email', 'password' ];
	protected $beforeInsert = [ 'beforeInsert' ];

	protected function beforeInsert( array $data ) {
		$data                       = $this->getPasswordHash( $data );
		$data['data']['created_on'] = date( 'Y-m-d H:i:s' );

		return $data;
	}

	protected function getPasswordHash( array $data ) {
		if( isset( $data['data']['password'] ) ) {
			$data['data']['password'] = password_hash( $data['data']['password'], PASSWORD_DEFAULT );
		}

		return $data;
	}

}