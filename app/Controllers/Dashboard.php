<?php

namespace App\Controllers;

use App\Models\Files;

define( 'FILEPATH', './upload_images/' );

class Dashboard extends BaseController {

	public function index() {

		$data = [];
		if( !session()->get( 'isLoggedIn' ) ) {
			return redirect()->to( '/' );
		}
		$model         = new Files();
		$data['files'] = $model->where( 'user_id', session()->get( 'id' ) )
			->orderBy( 'created_on', 'desc' )
			->findAll();
		echo view( 'templates/header' );
		echo view( 'dashboard', $data );
		echo view( 'templates/footer' );
	}

	public function uploadDocument() {
		if( $this->request->isAJAX() ) {
			$valid = true;
			try {
				$file = $this->request->getFiles()['file'];
				if( $file->isValid() && $file->move( FILEPATH ) ) {
					$model    = new Files();
					$fileInfo = pathinfo( $this->request->getFiles()['file']->getName() );
					$newFile  = [
						'user_id'   => session()->get( 'id' ),
						'name'      => $fileInfo['basename'],
						'file_size' => number_format( $this->request->getFiles()['file']->getSize() / 1048576, 2 ),
						'ext'       => strtoupper( $fileInfo['extension'] ),
						'file_path' => FILEPATH . $fileInfo['basename']

					];
					if( $model->save( $newFile ) ) {
						$model         = new Files();
						$data['files'] = $model->where( 'user_id', session()->get( 'id' ) )
							->orderBy( 'created_on', 'desc' )
							->findAll();
						$message       = view( 'list', $data );
					} else {
						if( file_exists( FILEPATH . $fileInfo['basename'] ) ) {
							unlink( FILEPATH . $fileInfo['basename'] );
						}
						$message = 'Failed to uplode the file,please try again!';
					}
				}
			} catch( \Exception $e ) {
				$message = $e->getMessage();
				$valid   = false;
			}

			if( false == $valid ) {
				$ajaxResponse = json_encode( [ 'error' => $message ] );
			} else {
				$ajaxResponse = json_encode( [ 'success' => $message ] );
			}
			echo $ajaxResponse;
		} else {
			return redirect()->to( 'dashboard' );
		}
	}

	public function deleteDocument() {
		if( $this->request->isAJAX() ) {
			$id    = $this->request->getpost( 'id' );
			$model = new Files();
			$valid = true;
			try {
				$file = $model->where( 'id', $id )
					->where( 'user_id', session()->get( 'id' ) )
					->findAll();
				if( file_exists( $file[0]['file_path'] ) && unlink( $file[0]['file_path'] ) && $model->where( 'id', $id )->delete() ) {
					$data['files'] = $model->where( 'user_id', session()->get( 'id' ) )
						->orderBy( 'created_on', 'desc' )
						->findAll();
					$message       = view( 'list', $data );
				} else {
					$valid   = false;
					$message = 'Failed to delete file!,Please try again Later';
				}
			} catch( \Exception $e ) {
				$message = $e->getMessage();
				$valid   = false;
			}

			if( false == $valid ) {
				$ajaxResponse = json_encode( [ 'error' => $message ] );
			} else {
				$ajaxResponse = json_encode( [ 'success' => $message ] );
			}
			echo $ajaxResponse;
		} else {
			return redirect()->to( 'dashboard' );
		}
	}

	public function searchDocument() {
		if( $this->request->isAJAX() ) {
			$search = $this->request->getpost( 'search' );
			$model  = new Files();
			$valid  = true;
			try {
				$where         = " user_id = " . session()->get( 'id' ) . " AND ( name like '%" . $search . "%' OR ext like '%" . $search . "%')";
				$data['files'] = $model->where( $where )
					->orderBy( 'created_on', 'desc' )
					->findAll();
				$message       = view( 'list', $data );
			} catch( \Exception $e ) {
				$message = $e->getMessage();
				$valid   = false;
			}

			if( false == $valid ) {
				$ajaxResponse = json_encode( [ 'error' => $message ] );
			} else {
				$ajaxResponse = json_encode( [ 'success' => $message ] );
			}
			echo $ajaxResponse;
		} else {
			return redirect()->to( 'dashboard' );
		}
	}

	public function updateFilename() {
		if( $this->request->isAJAX() ) {
			$id    = $this->request->getpost( 'id' );
			$name  = $this->request->getpost( 'name' );
			$valid = true;
			$model = new Files();
			try {

				$fileDetail  = $model->where( 'id', $id )
					->where( 'user_id', session()->get( 'id' ) )
					->findAll();
				$oldFileName = $fileDetail[0]['name'];
				$newFilName  = pathinfo( $name, PATHINFO_BASENAME ) . '.' . strtolower( $fileDetail[0]['ext'] );
				if( file_exists( FILEPATH . $newFilName ) ) {
					$valid   = false;
					$message = "The file with same name already exists";
				} else {
					if( file_exists( $fileDetail[0]['file_path'] ) ) {
						$dir = opendir( FILEPATH );
						// loop through all the files in the directory
						while( false !== ( $file = readdir( $dir ) ) ) {
							if( $file == $oldFileName ) {
								rename( FILEPATH . $oldFileName, FILEPATH . $newFilName );
							}
						}
						if( true == $valid ) {
							$model = new Files();
							$data  = [
								'user_id'   => session()->get( 'id' ),
								'name'      => $newFilName,
								'file_size' => $fileDetail[0]['file_size'],
								'ext'       => $fileDetail[0]['ext'],
								'file_path' => FILEPATH . $newFilName

							];
							if( $model->update( $id, $data ) ) {
								$data          = [];
								$data['files'] = $model->where( 'user_id', session()->get( 'id' ) )
									->orderBy( 'created_on', 'desc' )
									->findAll();
								$message       = view( 'list', $data );
							} else {
								$valid   = false;
								$message = "Failed to update database";
							}
						}
					} else {
						$valid == false;
						$message = "The file does not exists";
					}
				}
			} catch( \Exception $e ) {
				$message = $e->getMessage();
				$valid   = false;
			}

			if( false == $valid ) {
				$ajaxResponse = json_encode( [ 'error' => $message ] );
			} else {
				$ajaxResponse = json_encode( [ 'success' => $message ] );
			}
			echo $ajaxResponse;
		} else {
			return redirect()->to( 'dashboard' );
		}
	}

	public function downloadFile() {
		if( $this->request->getMethod() == 'post' ) {
			$id    = $this->request->getpost( 'id' );
			$valid = true;
			$model = new Files();
			try {
				$fileDetail = $model->where( 'id', $id )
					->where( 'user_id', session()->get( 'id' ) )
					->findAll();
				$filePath   = $fileDetail[0]['file_path'];
				if( isset( $filePath ) && file_exists( $filePath ) ) {
					header( 'Content-Description: File Transfer' );
					header( 'Content-Type: application/octet-stream' );
					header( 'Content-Disposition: attachment; filename="' . basename( $filePath ) . '"' );
					header( 'Expires: 0' );
					header( 'Cache-Control: must-revalidate' );
					header( 'Pragma: public' );
					header( 'Content-Length: ' . filesize( $filePath ) );
					flush(); // Flush system output buffer
					readfile( $filePath );
					die();
				} else {
					$valid   = false;
					$message = 'file download sucesfully!';
				}
			} catch( \Exception $e ) {
				$message = $e->getMessage();
				$valid   = false;
			}
			if( false == $valid ) {
				$session = session();
				$session->setFlashdata( 'error', 'Failed to download document, please try again!' );

				return redirect()->to( 'dashboard' );
			}
		} else {
			return redirect()->to( 'dashboard' );
		}
	}

}