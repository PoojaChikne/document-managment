<?php

namespace App\Controllers;

use App\Models\User;

class Login extends BaseController {

	public function index() {
		$data = [];
		helper( [ 'form' ] );

		if( session()->get( 'isLoggedIn' ) ) {
			return redirect()->to( 'dashboard' );
		}
		if( $this->request->getMethod() == 'post' ) {
			//let's do the validation here
			$rules = [
				'email'    => 'trim|required|min_length[6]|max_length[200]|valid_email',
				'password' => 'trim|required|min_length[8]|max_length[200]|validateUser[email,password]',
			];

			$errors = [
				'password' => [
					'validateUser' => 'Email or Password don\'t match'
				]
			];

			if( !$this->validate( $rules, $errors ) ) {
				$data['validation'] = $this->validator;
			} else {
				$model = new User();

				$user = $model->where( 'email', $this->request->getpost( 'email' ) )->first();

				$this->setUserSession( $user );

				return redirect()->to( 'dashboard' );

			}
		}

		echo view( 'templates/header', $data );
		echo view( 'login' );
		echo view( 'templates/footer' );
	}

	public function register() {
		$data = [];
		helper( [ 'form' ] );
		if( session()->get( 'isLoggedIn' ) ) {
			return redirect()->to( 'dashboard' );
		}
		if( $this->request->getMethod() == 'post' ) {
			//Adding some validation here on input filed
			$validations = [
				'firstname'        => 'trim|required|min_length[3]|max_length[250]',
				'lastname'         => 'trim|required|min_length[3]|max_length[250]',
				'email'            => 'trim|required|min_length[8]|max_length[250]|valid_email|is_unique[user.email]',
				'password'         => 'trim|required|min_length[5]|max_length[250]',
				'password_confirm' => 'matches[password]',
			];

			if( !$this->validate( $validations ) ) {
				//Not provided valid data, show the messages as per the validations given above
				$data['validation'] = $this->validator;
			} else {
				//Store user details in db and redirect to login screen
				$model = new User();

				$newUser = [
					'firstname' => $this->request->getpost( 'firstname' ),
					'lastname'  => $this->request->getpost( 'lastname' ),
					'email'     => $this->request->getpost( 'email' ),
					'password'  => $this->request->getpost( 'password' ),
				];
				$model->save( $newUser );
				$session = session();
				//TODO remove session
				$session->setFlashdata( 'success', 'Registration Successful, You can login now!' );

				return redirect()->to( '/' );

			}
		}
		echo view( 'templates/header', $data );
		echo view( 'register' );
		echo view( 'templates/footer' );
	}

	public function logout() {
		session()->destroy();

		return redirect()->to( '/' );
	}

	private function setUserSession( $user ) {
		$data = [
			'id'         => $user['id'],
			'firstname'  => $user['firstname'],
			'lastname'   => $user['lastname'],
			'email'      => $user['email'],
			'isLoggedIn' => true,
		];

		session()->set( $data );

		return true;
	}
}